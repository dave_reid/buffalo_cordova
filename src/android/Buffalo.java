package com.wmode.dreid.buffalo.cordova;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.StringWriter;
import java.io.PrintWriter;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

public class Buffalo extends CordovaPlugin {
    public static final String ACTION_GET_INSTALLED_APPS = "getInstalledApps";
    public static final String ACTION_SET_APP_ENABLED_SETTING = "setAppEnabledSetting";

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        try
        {
            if (ACTION_GET_INSTALLED_APPS.equals(action))
            {
                String json = this.callActionGetInstalledApps(args);
                callbackContext.success(json);
                return true;
            }
            else if(ACTION_SET_APP_ENABLED_SETTING.equals(action))
            {
                String json = this.callActionSetAppEnabledSetting(args);
                callbackContext.success(json);
                return true;
            }
            else
            {
                callbackContext.error("Invalid action");
                return false;
            }
        }
        catch(Exception e)
        {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            callbackContext.error("Exception thrown: " + sw.toString());
            return false;
        }
    }

    protected String callActionGetInstalledApps(JSONArray args) throws Exception, JSONException {
        JSONObject arg_object = args.getJSONObject(0);
        String packageNameFilter = arg_object.getString("packageNameFilter");
        
        PackageManager pm = this.getPM();
        JSONObject result = new JSONObject().put("packageNameFilter", packageNameFilter);
        JSONArray misses = new JSONArray();
        JSONObject matches = new JSONObject();
        result.put("misses", misses);
        result.put("matches", matches);
        
        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
        
        // filter and get AppEnabledSetting
        for (ApplicationInfo appInfo : packages)
        {
            String name = appInfo.packageName;
            int setting = pm.getApplicationEnabledSetting (name);
            if (name.matches(packageNameFilter))
            {
                matches.put(name, setting);
            }
            else
            {
                misses.put(name);
            }
        } 
        
        return result.toString();
    }
    protected String callActionSetAppEnabledSetting(JSONArray args) throws Exception, JSONException {
        JSONObject arg_object = args.getJSONObject(0);
        String packageName = arg_object.getString("packageName");
        int newState = arg_object.getInt("newState");
        int flags = arg_object.getInt("flags");
        
        PackageManager pm = this.getPM();
        
        int oldSetting = pm.getApplicationEnabledSetting (packageName);
        pm.setApplicationEnabledSetting (packageName, newState, flags);
        int newSetting = pm.getApplicationEnabledSetting (packageName);
        return "Success, changed from " + oldSetting + " to " + newSetting;
    }
    
    protected PackageManager getPM()
    {
        Context ctx = cordova.getActivity();
        return ctx.getPackageManager();
    }
}
