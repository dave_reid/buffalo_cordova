var buffalo = {
    getInstalledApps: function(packageNameFilter, successCallback, errorCallback) {
        cordova.exec(
            successCallback, // success callback function
            errorCallback, // error callback function
            'Buffalo', // mapped to our native Java class called "BuffaloPlugin"
            'getInstalledApps', // with this action name
            [{
                "packageNameFilter": packageNameFilter
            }]
        );
    },
    setAppEnabledSetting: function(packageName, newState, flags, successCallback, errorCallback) {
        cordova.exec(
            successCallback, // success callback function
            errorCallback, // error callback function
            'Buffalo', // mapped to our native Java class called "BuffaloPlugin"
            'setAppEnabledSetting', // with this action name
            [{
                "packageName": packageName,
                "newState" : newState,
                "flags" : flags
            }]
        );
    }
}
module.exports = buffalo;